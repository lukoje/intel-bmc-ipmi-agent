from .api.sel import SELTable
from .api.ibmc import IBMCApi
from .api.exceptions import *
from .log import Log
from .config import Configuration
from .process import Process


import threading
import time


class Agent(threading.Thread):

    def __init__(self,
                 host_name: str,
                 zabbix_server: str,
                 zabbix_port: int,
                 url: str,
                 username: str,
                 password: str) -> None:
        super(Agent, self).__init__()

        self.host_name: str = host_name
        self.zabbix_server: str = zabbix_server
        self.zabbix_port: int = zabbix_port
        self.url: str = url
        self._username: str = username
        self._password: str = password

        self.api = IBMCApi(server=self.url)
        self.sel_table = SELTable()

        self.is_active = True

    def run(self):
        while self.is_active:

            if not self.api.is_connected:

                Log.app.debug(
                    f"Device {self.host_name} is not logged in. Try to login...")
                self.login()
                continue

            self.proceed()

            self.api.check_session_timeout(reset=True)
            time.sleep(Configuration.poll_delay)

    def proceed(self):
        data = self.api.get_sel()
        self.sel_table.update_data(data)

        new_data = self.sel_table.get_record_from_queue()

        if new_data:
            Log.app.debug('Recived new event!')
            json_data = new_data.as_json()
            command = f"""zabbix_sender -z {self.zabbix_server} -p {str(self.zabbix_port)} -s {self.host_name} -k 'ibmc.event.message' -o '{json_data}'"""
            Log.app.debug(f"Send new data using: {command}")
            stdout, stderr = Process.call(command)
            if stderr:
                Log.app.warn(
                    f'Oops. Something went wrong when I ran zabbix_sender ({stderr})')

    def __repr__(self) -> str:
        return f'<Agent(host_name={self.host_name}, zabbix_server={self.zabbix_server}, zabbix_port={self.zabbix_port}, url={self.url}, username={self._username}, password={self._password})>'

    def login(self):
        LOGIN_ATTEMPTS = 3

        for attempt in range(LOGIN_ATTEMPTS):
            if not self._username or not self._password:
                Log.app.error(f"Invalid username or password")

            try:
                self.api.auth(name=self._username, pwd=self._password)
            except AuthenticationError as error:
                Log.app.error(error)
            except TooManySessionError as error:
                Log.app.error(error)
            except ReceviedEmtpyResponse as error:
                Log.app.error(error)

        return

    def logout(self):
        Log.app.info(f'Logout from {self.host_name}')
        self.is_active = False
        self.api.logout()

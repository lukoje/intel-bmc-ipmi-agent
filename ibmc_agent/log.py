import os
import logging

class Log:
    initialized: bool = False
    app = logging.getLogger('app')
    api = logging.getLogger('bmc-web-api')

    @classmethod
    def init(cls, log_file: str = None, log_level: int = logging.INFO):
        cls.log_file = log_file
        cls.log_level = log_level
        
        if cls.initialized:
            return
        
        logging.basicConfig(level=log_level)
        if cls.log_file and os.path.exists(cls.log_file):
            pass
        
        cls.initialized = True


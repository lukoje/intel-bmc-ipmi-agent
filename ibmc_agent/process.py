from subprocess import Popen, PIPE
import os
import signal
import time


class Process(object):

    @staticmethod
    def devnull():
        return open('/dev/null', 'w')

    @staticmethod
    def call(command: list, cwd: str = None, shell: bool = False) -> tuple[bytes, bytes]:
        if type(command) is not str or ' ' in command or shell:
            shell = True
        else:
            shell = False

        pid = Popen(command, cwd=cwd, stdout=PIPE, stderr=PIPE, shell=shell)
        pid.wait()
        (stdout, stderr) = pid.communicate()

        if type(stdout) is bytes:
            stdout = stdout.decode('utf-8')
        if type(stderr) is bytes:
            stderr = stderr.decode('utf-8')

        return (stdout, stderr)

    @staticmethod
    def exists(program):
        p = Process(['which', program])
        stdout = p.stdout().strip()
        stderr = p.stderr().strip()

        if stdout == '' and stderr == '':
            return False

        return True

    def __init__(self,
                 command: list,
                 devnull: bool = False,
                 stdin: int = PIPE,
                 stdout: int = PIPE,
                 stdeer: int = PIPE,
                 cwd: str = None,
                 bufsize: int = 0) -> None:

        if isinstance(command, str):
            command = command.split(' ')

        self.command = command

        self.out = None
        self.err = None

        if devnull:
            sout = Process.devnull()
            serr = Process.devnull()
        else:
            sout = stdout
            serr = stdeer

        self.pid = Popen(command, cwd=cwd, bufsize=bufsize,
                         stdin=stdin, stdout=sout, stderr=serr)

    def __del__(self):
        try:
            if self.pid and self.pid.poll() is None:
                self.interrupt()
        except AttributeError:
            pass

    def interrupt(self, wait_time: float = 2.0):
        try:
            pid = self.pid.pid
            cmd = self.command

            if isinstance(cmd, list):
                cmd = ' '.join(cmd)

            os.kill(pid, signal.SIGINT)

            start_time = time.time()

            while self.pid.poll() is None:
                time.sleep(0.1)
                if time.time() - start_time > wait_time:
                    os.kill(pid, signal.SIGTERM)
                    self.pid.terminate()
                    break
        except OSError as error:
            return error

    def get_output(self) -> tuple[str, str]:
        if self.pid.poll() is None:
            self.pid.wait()

        if self.out is None:
            (self.out, self.err) = self.pid.communicate()

        if type(self.out) is bytes:
            self.out = self.out.decode('utf-8')

        if type(self.err) is bytes:
            self.err = self.err.decode('utf-8')

        return (self.out, self.err)

    def poll(self) -> int:
        return self.pid.poll()

    def wait(self) -> int:
        return self.pid.wait()

    def running_time(self) -> float:
        return time.time() - self.start_time

    def stdout(self) -> str:
        self.get_output()
        return self.out

    def stderr(self) -> str:
        self.get_output()
        return self.err

    def stdin(self, text: str) -> None:
        if self.pid.stdin:
            self.pid.stdin.write(text.encode('utf-8'))
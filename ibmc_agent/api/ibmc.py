import base64
import urllib3
import pickle
import os

from xml.etree import ElementTree as xmltree
from xml.etree.ElementTree import ParseError
from requests import Session
from typing import NamedTuple, Optional
from datetime import datetime, timedelta
from ibmc_agent.config import Configuration
from ibmc_agent.log import Log
from .exceptions import *

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
SESSION_TIMEOUT = 30


class SessionCache(NamedTuple):
    session: Session
    token: str
    host_sid: str
    expired: datetime


class IBMCApi:

    def __init__(self,
                 server: str = 'https://localhost/',
                 session: Optional[Session] = None) -> None:

        self.session: Session = session or Session()
        self.session.verify = False
        self.session.headers.update({
            "Content-Type": "application/xml",
            "Cache-Control": "no-cache"
        })

        if not server.endswith("/cgi"):
            server = server.rstrip('/') + '/cgi'

        self.url: str = server
        self.token = None
        self.host_sid = None
        self.session_cache = None
        self._name = None
        self._pwd = None

    @property
    def is_connected(self):
        result = self.heartbeat().find('./RESULT')

        if not isinstance(result, xmltree.Element) or result is None:
            return False

        status = result.text

        if not status or status != 'OK':
            return False

        return True

    def is_token_valid(self, response: xmltree.Element) -> bool:
        result = response.find('./RESULT')
        if result is not None and result.text == 'TOKEN_INVALID':
            return False
        return True

    def request(self, method_url: str, data: str, method='POST') -> xmltree.Element:
        url = self.url.rstrip("/") + method_url

        if data:
            data = data.encode('utf-8')

        if method == 'PUT':
            resp = self.session.put(url, data=data)
        elif method == 'GET':
            resp = self.session.get(url)
        else:
            if data:
                resp = self.session.post(url, data=data)
            else:
                resp = self.session.post(url)

        if not resp.text:
            Log.api.warning("Received empty response")
            return

        try:
            result = xmltree.fromstring(resp.text)
        except ParseError as error:
            Log.api.warning(error)
            return None

        if not self.is_token_valid(response=result):
            Log.api.warning("Current token is invalid. Authentication ...")
            self.session_cache = None
            self.remove_session_cache()
            self.auth(self._name, self._pwd)
        return result

    def remove_session_cache(self, filename=Configuration.api_session_file) -> None:
        if not os.path.exists(filename):
            return
        os.remove(filename)
        Log.api.info(f'Cache file: {filename} removed.')

    def create_session_cache(self, filename=Configuration.api_session_file) -> None:
        if not self.session_cache:
            Log.api.info(f"Session cache not found! Try login again")
            return

        with open(filename, 'wb') as cache_file:
            pickle.dump(self.session_cache, cache_file)
            Log.api.info(f"Create session cache {filename}")

    def read_session_cache(self, filename=Configuration.api_session_file) -> None:
        if not os.path.exists(filename):
            Log.api.info(f"Session cache file {filename} not found")
            return

        with open(filename, 'rb') as cache_file:
            data = pickle.load(cache_file)
        return data if data else None

    def _login_using_session_cache(self) -> None:
        self.session_cache = self.read_session_cache()

        if not self.session_cache:
            Log.api.info(
                f"Can't login via session cache. Session file: {self.session_cache}")
            return

        time = datetime.now()
        expired = self.session_cache.expired
        if ((expired - time).seconds // 60) % 60 <= 0:
            pass
            Log.api.info(
                f"Can't login via session cache. Session file expired.")

        self.session = self.session_cache.session
        self.token = self.session_cache.token
        self.host_sid = self.session_cache.host_sid

        # Check connectivity
        if not self.is_connected:
            Log.api.info(
                f"Can't login via session cache. Session file: {self.session_cache}")
            return False

        return True

    def auth(self, name: str, pwd: str):
        self._name = name
        self._pwd = pwd
        # self._login_using_session_cache()

        if self.is_connected:
            return

        url = self.url.rstrip("/") + "/login.cgi"

        encoded_pwd = base64.b64encode(pwd.encode())
        payload = {
            'name': name,
            'pwd': pwd,
            'encodedpwd': encoded_pwd.decode()
        }

        self.session.post(url, data=payload, allow_redirects=True)

        hostsid = self.session.cookies.get('__Host-SID')

        if not hostsid:

            if self.session.cookies.get('__Host-AVAILABLE_SESSION') == '-1':
                raise TooManySessionError
            raise AuthenticationError('Cookie "__Host-SID" is empty.')

        self.host_sid = hostsid
        Log.api.info(f"Received HOST_SID: {self.host_sid} for {self.url}")

        token = self.request_csrf_token()
        if (token.find('RESULT').text != 'OK'):
            raise AuthenticationError('Can\'t receive token.')

        self.token = token.find('TOKEN').text
        self.session.cookies.set('__Host-Authenticated', '1')

        Log.api.debug("Received TOKEN: %s" % self.token)

        expired = datetime.now() + timedelta(minutes=SESSION_TIMEOUT)
        self.session_cache = SessionCache(
            session=self.session,
            token=self.token,
            host_sid=self.host_sid,
            expired=expired)

        # self.create_session_cache()

    def logout(self) -> xmltree.Element:
        resp = self.request('/logout.cgi', method='GET', data=None)
        self.session.cookies.set('__Host-Authenticated', '')
        #self.session.close()
        return resp

    def request_csrf_token(self) -> xmltree.Element:
        """ 
            <?xml version="1.0"?>
                <IPMI>
                <RESULT>OK</RESULT>
                <TOKEN>TOKEN</TOKEN>
            </IPMI>
        """
        data = "<?xml version=\"1.0\"?>\n<IPMI><REQUEST>GENERATE</REQUEST></IPMI>\n"
        resp = self.request('/csrf_tokens.cgi', data=data, method='PUT')
        return resp

    def heartbeat(self) -> xmltree.Element:
        data = f"<?xml version=\"1.0\"?>\n<IPMI><TOKEN>{self.token}</TOKEN><PRIV>04</PRIV></IPMI>\n"
        resp = self.request('/heartbeat.cgi', data=data)
        return resp

    def get_system_info(self) -> xmltree.Element:
        data = f"<?xml version=\"1.0\"?>\n<IPMI><TOKEN>{self.token}</TOKEN><PRIV>04</PRIV></IPMI>\n"
        resp = self.request('/getsysteminfo.cgi', data=data)
        return resp

    def get_host_power_status(self) -> xmltree.Element:
        """
            <?xml version="1.0"?>
            <IPMI>
            <COMPLETION_CODE>00</COMPLETION_CODE>
            <POWER_INFO>
                <POWER STATUS="ON"/>
            </POWER_INFO>
            </IPMI>
        """
        data = f"<?xml version=\"1.0\"?><POSTDATA><PRIV>04</PRIV><TOKEN>{self.token}</TOKEN><FUNCTION>SERVER_POWER_CONTROL</FUNCTION><PARAMETERS><GET_CHASSIS_STATUS>1</GET_CHASSIS_STATUS></PARAMETERS></POSTDATA>\n"
        resp = self.request('/server_power_control.cgi', data=data)
        return resp

    def get_sel(self, index=65535, count=5) -> xmltree.Element:
        """
            <?xml version="1.0"?>
            <IPMI>
            <SEL_INFO TOTAL_UNITS="4000" FREE_UNITS="0" SEL_HEAD="10599" SEL_TAIL="6600">
                <SEL RECORD_ID="" TIME="" CONTR="" MFR_ID="" SNSR_TYPE="" SNSR_ID="" SNSR_NAME="" DESCR="" SEVERITY=""/>
                <SEL RECORD_ID="" TIME="" CONTR="" MFR_ID="" SNSR_TYPE="" SNSR_ID="" SNSR_NAME="" DESCR="" SEVERITY=""/>
                <SEL RECORD_ID="" TIME="" CONTR="" MFR_ID="" SNSR_TYPE="" SNSR_ID="" SNSR_NAME="" DESCR="=" SEVERITY=""/>
            </SEL_INFO>
            </IPMI>
        """

        data = f"<?xml version=\"1.0\"?><IPMI><LANG>English</LANG><TOKEN>{self.token}</TOKEN><PRIV>04</PRIV><INDEX>{index}</INDEX><COUNT>{count}</COUNT></IPMI>\n"
        resp = self.request('/getsel.cgi', data=data)

        return resp

    def check_session_timeout(self, reset=False) -> xmltree.Element:
        if reset:
            data = "<?xml version=\"1.0\"?>\n<IPMI><SESSIONINFO><RESET>1</RESET></SESSIONINFO></IPMI>\n"
        else:
            data = "<?xml version=\"1.0\"?>\n<IPMI><SESSIONINFO></SESSIONINFO></IPMI>\n"

        resp = self.request('/check_session_timeout.cgi', data=data)
        return resp


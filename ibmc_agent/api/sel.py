from dataclasses import dataclass, field
from datetime import datetime
from xml.etree import ElementTree as xmltree

from ibmc_agent.config import Configuration
from ibmc_agent.log import Log


import json
import base64

@dataclass(unsafe_hash=True)
class SELRecord(object):

    record_id: int
    time: datetime
    controller: str
    mfr_id: str
    sensor_type: str
    sensor_id: str
    sensor_name: str
    description: str
    severity: str
    severity_level: int = field(default_factory=int)
    asserted: bool = field(default_factory=bool)

    def as_json(self):
        return json.dumps({
            'record_id': self.record_id,
            'time': self.time.strftime('%c'),
            'controller': self.controller,
            'mfr_id': self.mfr_id,
            'sensor_type': self.sensor_type,
            'sensor_id': self.sensor_id,
            'sensor_name': self.sensor_name,
            'description': self.description,
            'severity': self.severity,
            'severity_level': self.severity_level,
            'asserted': self.asserted
        })

class SELTable:
    SEL_INFO = './SEL_INFO'
    SEL = './SEL_INFO/SEL'

    def __init__(self):
        self.total_units: int = 0
        self.sel_head: int = 0
        self.sel_tail: int = 0
        self.records: list[SELRecord] = []
        self.queue: list[SELRecord] = []
        self._is_first_data = True

    def is_useful_record(self, record: SELRecord) -> bool:
        if Configuration.skip_non_critical and \
                'non-critical' in record.description.lower().split(' '):
            return False

        return True

    def add_record(self, record: SELRecord):
        self.records.append(record)

        if self.is_useful_record(record):
            Log.app.debug('Receive useless record. Skip it')
            self.queue.append(record)

        self.records.sort(key=lambda r: r.time, reverse=True)
        self.queue.sort(key=lambda r: r.time)

    def clear_queue(self):
        self.queue.clear()

    def get_record_from_queue(self) -> SELRecord:
        if self.queue:
            return self.queue.pop(0)
        return None

    def update_data(self, raw_xml_data: xmltree.Element):

        header = raw_xml_data.find(self.SEL_INFO)

        if header is None:
            return

        self.total_units = int(header.attrib['TOTAL_UNITS'])
        self.sel_head = int(header.attrib['SEL_HEAD'])
        self.sel_tail = int(header.attrib['SEL_TAIL'])

        sel_rows = raw_xml_data.findall(self.SEL)

        for row in sel_rows:
            record = SELRecord(
                record_id=int(base64.b64decode(
                    row.attrib['RECORD_ID']).decode()),
                time=datetime.strptime(
                    base64.b64decode(row.attrib['TIME']).decode(),
                    '%a %b  %d %H:%M:%S %Y'),
                controller=base64.b64decode(row.attrib['CONTR']).decode(),
                mfr_id=base64.b64decode(row.attrib['MFR_ID']).decode(),
                sensor_type=base64.b64decode(row.attrib['SNSR_TYPE']).decode(),
                sensor_id=base64.b64decode(row.attrib['SNSR_ID']).decode(),
                sensor_name=base64.b64decode(row.attrib['SNSR_NAME']).decode(),
                description=base64.b64decode(row.attrib['DESCR']).decode(),
                severity=base64.b64decode(row.attrib['SEVERITY']).decode()
            )

            if record.severity == 'OK':
                record.severity = 'Informational'
                record.severity_level = 0
            elif record.severity == 'Non-Critical':
                record.severity = 'Warning'
                record.severity_level = 1
            elif record.severity == 'Critical':
                record.severity = 'Critical'
                record.severity_level = 2
            else:
                record.severity = 'Unknown'
                record.severity_level = 0

            if 'asserted' in record.description.lower().split(' '):
                record.asserted = True

            if record not in self.records:
                self.add_record(record)

        if self._is_first_data:
            Log.app.debug('Records and Queue are same. Clear queue ...')
            self.clear_queue()
            self._is_first_data = False
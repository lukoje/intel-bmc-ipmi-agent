
class TooManySessionError(Exception):
    def __init__(self, message="Too many session. Try again later") -> None:
        super().__init__(message)


class AuthenticationError(Exception):
    pass


class ReceviedEmtpyResponse(Exception):
    pass


class InvalidTokenError(Exception):
    pass

import yaml
import os
from .log import Log

class Configuration:

    initialized: bool = False
    config_file: str = '/etc/default/ibmc-agent-config.yaml'
    api_session_file: str = None
    skip_non_critical: bool = False
    poll_delay: int = 5
    devices: list = []

    @classmethod
    def init(cls, config_file: str = None, arguments=None):
        # Only initialize configuration class once
        if cls.initialized:
            return
        
        cls.config_file = config_file or Configuration.config_file
        cls.arguments = arguments

        cls.initialized = True

        Configuration.load_from_config_file()
        Configuration.load_from_arguments()

    @classmethod
    def load_from_config_file(cls):
        """ Load configuration from file """
        if not os.path.exists(Configuration.config_file):
            Log.app.error("Can't load configuration file. File doesn't exists!")
            return
        
        config = None
        with open(cls.config_file, 'r') as yaml_file:
            config = yaml.safe_load(yaml_file)
        
        if config is None or \
             'devices' not in config:
            Log.app.error('Configuration syntax error!')
            return

        cls.devices = config['devices']

        if 'agent' in config:
            cls.skip_non_critical = bool(config['agent']['skip_non_critical'])
            cls.poll_delay = int(config['agent']['poll_delay'])
        

    @classmethod
    def load_from_arguments(cls):
        cls.skip_non_critical: bool = bool(cls.arguments.skip_non_critical or Configuration.skip_non_critical)
        cls.poll_delay: int = int(cls.arguments.poll_delay or Configuration.poll_delay)
        

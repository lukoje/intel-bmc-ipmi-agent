# -*- coding: utf-8 -*-


__version__ = "0.3.0"

from .log import Log
from .config import Configuration
from .agent import Agent
from argparse import ArgumentParser, BooleanOptionalAction
import sys
import signal

class Application:
    def __init__(self) -> None:
        self.is_active = False
        
        signal.signal(signal.SIGINT, self._exit_graceful)
        signal.signal(signal.SIGTERM, self._exit_graceful)

        self.devices: list[Agent] = []

    def _exit_graceful(self, *args):
        Log.app.info('Stopping agent process ...')
        self.shutdown()
        self.is_active = False
        Log.app.info('Successfuly logged out. Exit now')

    def shutdown(self):
        Log.app.info('Logout from all devices ...')
        for device in self.devices:
            device.is_active = False
            device.logout()

    def init(self):
        Log.app.info('Initialize agents ...')
        self.is_active = True
        
        if Configuration.devices:
            self.devices = [
                Agent(
                    host_name=dev['host']['name'],
                    zabbix_server=dev['host']['zabbix-server'],
                    zabbix_port=dev['host']['zabbix-port'],
                    url=dev['host']['url'],
                    username=dev['host']['username'],
                    password=dev['host']['password']
                )
                for dev in Configuration.devices]
        

    def recreate_device(self, device: Agent) -> Agent:
        device.is_active = False
        new_device = Agent(
            host_name=device.host_name,
            zabbix_server=device.zabbix_server,
            zabbix_port=device.zabbix_port,
            url=device.url,
            username=device._username,
            password=device._password
        )
        return new_device

    def run(self):
        Log.app.info('Starting application ...')
        self.init()

        for device in self.devices:
            Log.app.info(f'Run agent on {device.host_name}')
            device.start()

        while self.is_active:
            for device in self.devices:
                device_thread_status = device.is_alive()
                if self.is_active and (not device_thread_status):
                    Log.app.warn(
                        f"Agent {device} was stop. Try to recreate it")
                    device.logout()

                    index = self.devices.index(device)
                    new_device = self.recreate_device(device)
                    self.devices[index] = new_device
                    self.devices[index].is_active = True
                    self.devices[index].start()


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('-c', '--config',
        help="Configuration file",
        required=False
    )
    parser.add_argument('-L', '--log-file',
        help="Log file",
        required=False
    )
    parser.add_argument('--skip-non-critical',
        action=BooleanOptionalAction,
        help="Skip non-critical warning. Override same config option.",
        required=False
    )
    parser.add_argument('--poll-delay',
        help="Delay between polls. Override same config option.",
        required=False
    )

    args = parser.parse_args()
    return args

def main():
    args = parse_arguments()

    Log.init(log_file=args.log_file)
    Configuration.init(config_file=args.config, arguments=args)

    app = Application()
    app.run()

    return 0
# -*- coding: utf-8 -*-


from setuptools import setup
from ibmc_agent import __version__


setup(
    name="intel-bmc-agent",
    packages=["ibmc_agent"],
    install_requires=['requests', 'pyyaml'],
    entry_points={
        "console_scripts": ['intel-bmc-agent = ibmc_agent.app:main']
    },
    version=__version__,
    description="Intel BMC IPMI Agent",
    author="Vadim Trionov",
    author_email="retroarefobia@gmail.com",
)

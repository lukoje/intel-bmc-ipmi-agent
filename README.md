# Intel BMC Event Agent

Агент для мониторинга событий на серверах Intel через IMPI и отправки их 
на Zabbix сервер.

## Требования
* Python >= 3.9
* zabbix_sender

## Установка

1. `python setup.py install` или `pip install .`
2. Скопировать и сконфигурировать *-config.yaml в /etc/default
3. Скопировать ibmc-agent.service в /lib/systemd/system
4. Выполнить systemctl daemon-reload

## Запуск

```
intel-bmc-agent -c /etc/default/ibmc-config.yaml
или используя systemd
systemctl enable ibmc-agent.service
systemctl start ibmc-agent.service
```
## Zabbix
Агент отправляет сообщения в формате JSON на Zabbix используя ключ `ibmc.event.message`.
Поэтому необходимо создать элемент данных, который принимает `ibmc.event.message` и парсить из него все, что нужно :)

## Usage
```
usage: intel-bmc-agent [-h] [-c CONFIG] [-L LOG_FILE] [--skip-non-critical | --no-skip-non-critical] [--poll-delay POLL_DELAY]

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        Configuration file
  -L LOG_FILE, --log-file LOG_FILE
                        Log file
  --skip-non-critical, --no-skip-non-critical
                        Skip non-critical warning. Override same config option.
  --poll-delay POLL_DELAY
                        Delay between polls. Override same config option.
```
